package com.devcamp;

public abstract class Shape {
    private String color;
    public abstract double getArea();
    public String toString() {
        return "Shape[color = " + this.color + "]";
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }    
}
