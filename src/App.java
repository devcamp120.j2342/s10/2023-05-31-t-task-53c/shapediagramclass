import com.devcamp.Rectangle;
import com.devcamp.Shape;
import com.devcamp.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle rectangle1 = new Rectangle(10, 5);
        Shape rectangle2 = new Rectangle(20, 12);

        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());

        rectangle1.setColor("red");
        rectangle2.setColor("yellow");

        System.out.println(rectangle1);
        System.out.println(rectangle2.toString());

        Triangle triangle1 = new Triangle(12, 4);
        Shape triangle2 = new Triangle(30, 5);

        System.out.println(triangle1.getArea());
        System.out.println(triangle2.getArea());

        triangle1.setColor("white");
        triangle2.setColor("black");

        System.out.println(triangle1);
        System.out.println(triangle2);
    }
}
